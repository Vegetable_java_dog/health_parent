/**
 * 笔记本的操作
 * @returns
 */
var base_path = "http://localhost:82";
//显示文章分类列表
function loadUserBooks() {
	//发送ajax请求
	$.ajax({
		url:base_path+"/book/loadBooks.do",
		type:"post",
		dataType:"json",
		success:function(result){
			//判断查询是否成功
			if(result.status==0){
				//获取笔记本集合
				var books=result.data;
				for(var i=0;i<books.length;i++){
					//获取笔记本ID
					var bookId=books[i].id;
					//获取笔记本名称
					var bookName=books[i].cn_notebook_name;
					
					//创建一个笔记本列表项的li元素
					createBookLi(bookId,bookName);
				}
			}
		},
		error:function(){
			alert("笔记本加载失败")
		}
	});
	
};
//创建一个笔记本列表项的li元素
function createBookLi(bookId,bookName){
	var sli="";
	sli+='<li class="online">';
	sli+='<a>';
	sli+='<i class="fa fa-book" title="online" rel="tooltip-bottom">';
	sli+='</i>';
	sli+=bookName;
	sli+='</a>';
	sli+='</li>';
	//将sli字符串转换成jquery对象li元素
	var $li=$(sli);
	//将bookId的值与jquery对象绑定
	$li.data("bookId",bookId);
	//将li元素添加到笔记本ul列表区
	$("#book_ul").append($li);
};


function addBook(){
	//获取笔记本标题
	var title=$("#input_notebook").val();
	//数据格式检查
	var ok=true;
	if(title==""){
		ok=false;
		$("#title_span").html("标题不能为空");
	}
	if(ok){
		//发送ajax请求
		$.ajax({
			url:base_path+"/book/add.do",
			type:"post",
			data:{"title":title},
			dataType:"json",
			success:function(result){
				var book=result.data;
				if(result.status==0){
					var id=book.cn_notebook_id;
					var title=book.cn_notebook_name;
					createBookLi(id,title);//创建一个笔记本列表的li元素
					alert(result.msg);
				}
			},
			error:function(){
				alert("创建笔记本失败");
			}
		});
	}
}; 
