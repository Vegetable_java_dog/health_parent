package com.itlin.controller;


import com.itlin.utils.QiniuUtils;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@Controller
@RequestMapping("/system")
public class UMeditorController {
    @RequestMapping("/UMeditor.do")
    @ResponseBody
    public void umeditorUpload(@RequestParam("upfile") MultipartFile file,HttpServletRequest request,HttpServletResponse response)
            throws IllegalStateException, IOException{
        JSONObject json = new JSONObject();
        String type="."+file.getOriginalFilename().split("\\.")[1];
        String name=System.currentTimeMillis()+file.getOriginalFilename();
        QiniuUtils.upload2Qiniu(file.getBytes(),name);
        String src="http://qr4k8o67g.hn-bkt.clouddn.com/"+name;

        json.put("state","SUCCESS");
        json.put("original",file.getOriginalFilename());
        json.put("size",file.getSize());
        json.put("url",src);
        json.put("title", name);
        json.put("type",type);


        response.setContentType("text/html; charset=UTF-8");
        PrintWriter writer=response.getWriter();
        writer.write(json.toString());
        writer.close();
    }
}
