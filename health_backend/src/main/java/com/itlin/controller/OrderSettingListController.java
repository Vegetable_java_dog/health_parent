package com.itlin.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itlin.constant.MessageConstant;
import com.itlin.entity.PageResult;
import com.itlin.entity.QueryPageBean;
import com.itlin.entity.Result;
import com.itlin.pojo.CheckItem;
import com.itlin.pojo.Order;
import com.itlin.pojo.OrderResult;
import com.itlin.service.OrderService;
import com.itlin.service.OrderSettingListService;
import com.itlin.utils.SMSUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 预约列表控制层
 */
@RestController
@RequestMapping("/ordersettinglist")
public class OrderSettingListController {
    @Reference
    private OrderSettingListService orderSettingListService;

    @Reference
    private OrderService orderService;

    // 日期格式化
    public static String formatDateStr(String dateString){
        String[] ts = dateString.split("T")[0].split("-");
        int number = Integer.parseInt(ts[2]);
        ++number;
        ts[2] = String.valueOf(number);
        return ts[0] + "-" + ts[1] + "-" + ts[2];
    }

    // 预约列表分页查询
    @PostMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean) {
        return orderSettingListService.pageQuery(queryPageBean);
    }


    // 添加一条新的预约记录
    @PostMapping("/add")
    public Result add(String setmealId, @RequestBody Map map){

        Result result = new Result(false, MessageConstant.ORDER_FAIL);

        //取出电话号码
        String telephone = (String) map.get("telephone");
        System.out.println("52 telephone="+telephone);

        try {
            map.put("setmealId",setmealId);

            // 格式化预约日期与生日的格式
            String str = (String) map.get("orderDate");
            map.put("orderDate", formatDateStr(str));

            str = (String) map.get("birthday");
            map.put("birthday", formatDateStr(str));

            // 进行预约的业务处理
            map.put("orderType", Order.ORDERTYPE_TELEPHONE);
            result = orderService.order(map);

        } catch (Exception e) {
            e.printStackTrace();
            return result;
        }

        if (result.isFlag()) {      // 预约成功  发送短信进行通知
            String orderDate = (String) map.get("orderDate");
            System.out.println("75 orderDate="+orderDate);
            try {
                // 发送预约成功通知
                SMSUtils.startdate(orderDate,telephone);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    // 确定预约 修改预约的状态
    @PutMapping("/updateStatus")
    public Result updateStatus(int orderId){
        try{
            orderSettingListService.updateStatusByOrderId(orderId);

            return new Result(true, MessageConstant.ORDER_SUCCESS);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.ORDER_FAIL);
        }
    }

    // 删除一条预约信息
    @PostMapping("/delete")
    public Result delete(@RequestBody Map map){
        try{
            orderSettingListService.delete(map);

            return new Result(true, MessageConstant.ORDER_CANCEL_SUCCESS);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.ORDER_CANCEL_FAIL);
        }
    }

    //编辑
    @RequestMapping("/addResults")
    public Result addResults(String doctor,@RequestBody OrderResult orderResult){
        try {
            orderResult.setDoctor(doctor);
            orderSettingListService.addResults(orderResult);
        }catch (Exception e){
            return new Result(false,MessageConstant.ADD_ORDERRESULT_FAIL);
        }
        return new Result(true,MessageConstant.ADD_ORDERRESULT_SUCCESS);
    }
}
