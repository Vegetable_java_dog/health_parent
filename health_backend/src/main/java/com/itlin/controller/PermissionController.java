package com.itlin.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itlin.constant.MessageConstant;
import com.itlin.entity.PageResult;
import com.itlin.entity.QueryPageBean;
import com.itlin.entity.Result;
import com.itlin.pojo.Permission;
import com.itlin.service.PermissionService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/permission")
public class PermissionController {

    @Reference
    private PermissionService permissionService;

    // 分页查询
    @PostMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
        return permissionService.pageQuery(queryPageBean);
    }

    // 添加一个新权限
    @PostMapping("/add")
    public Result add(@RequestBody Permission permission){
        try{
            permissionService.add(permission);

            return new Result(true, MessageConstant.ADD_PERMISSION_SUCCESS);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_PERMISSION_FAIL);
        }
    }

    // 查询所有的权限数据
    @GetMapping("/findAll")
    public Result findAll(){
        try{
            List<Permission> list =  permissionService.findAll();

            return new Result(true, MessageConstant.GET_PERMISSION_SUCCESS, list);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_PERMISSION_FAIL);
        }
    }
}
