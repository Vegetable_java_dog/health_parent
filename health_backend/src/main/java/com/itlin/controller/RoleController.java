package com.itlin.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itlin.constant.MessageConstant;
import com.itlin.entity.Result;
import com.itlin.pojo.Role;
import com.itlin.service.RoleService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/role")
public class RoleController {
    @Reference
    private RoleService roleService;


    // 查询所有的角色数据
    @GetMapping("/findAll")
    public Result findAll() {
        try {
            List<Role> list = roleService.findAll();

            return new Result(true, MessageConstant.GET_ROLE_SUCCESS, list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_ROLE_FAIL);
        }
    }

    // 根据角色id 查询包含的权限ids
    @GetMapping("/findPermissionsByRoleId")
    public Result getPermissionIds(Integer roleId){
        try{
            List<Integer> permissionIds = roleService.findPermissionsByRoleId(roleId);
            return new Result(true, MessageConstant.GET_PERMISSION_SUCCESS, permissionIds);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_PERMISSION_FAIL);
        }
    }

    // 更新角色所对应的权限
    @PutMapping("/updatePermissionAndRoleId")
    public Result updatePermissionAndRoleId(Integer roleId,@RequestBody Integer[] permissionIds){
        try{
            roleService.updatePermissionAndRoleId(roleId, permissionIds);

            return new Result(true, MessageConstant.SET_PERMISSION_SUCCESS);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.SET_PERMISSION_FAIL);
        }
    }
}
