package com.itlin.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itlin.pojo.Book;
import com.itlin.service.BookService;
import com.itlin.utils.NoteResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.List;

@Controller
@RequestMapping("/book")
public class BooksController {
    @Reference
    private BookService bookService;


    @ResponseBody
    @RequestMapping("/loadBooks.do")
    public NoteResult<List<Book>> execute(){
        NoteResult<List<Book>> result = bookService.loadUserBook();
        return result;
    }

    @ResponseBody
    @RequestMapping("/add.do")
    public NoteResult<Book> execute(String title){
        NoteResult<Book> result = bookService.addBook(title);
        return result;
    }
}
