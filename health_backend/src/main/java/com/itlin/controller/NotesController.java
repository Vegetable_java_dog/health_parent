package com.itlin.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itlin.pojo.Cover;
import com.itlin.pojo.Note;
import com.itlin.pojo.Share;
import com.itlin.service.NoteService;
import com.itlin.service.ShareService;
import com.itlin.utils.NoteResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/note")//匹配路径
public class NotesController {
    @Reference
    private NoteService noteService;
    @Reference
    private ShareService shareService;

    @RequestMapping("/loadnotes.do")
    @ResponseBody//以json格式返回
    public NoteResult<List<Map>> executeloadnotes(Integer bookId){
        NoteResult<List<Map>> result = noteService.loadBookNotes(bookId);
        return result;
    }

    @RequestMapping("/add.do")
    @ResponseBody
    public NoteResult<Note> executeadd(Integer bookId, String title){
        NoteResult<Note> result = noteService.addNote(bookId, title);
        return result;
    }

    @RequestMapping("/delete")
    @ResponseBody
    public NoteResult executedelete(Integer noteId){
        NoteResult result=noteService.deleteNote(noteId);
        return result;
    }

    @RequestMapping("/load.do")
    @ResponseBody//以json格式返回数据
    public NoteResult<Note> executeload(Integer noteId){
        NoteResult<Note> result = noteService.loadNote(noteId);
        return result;
    }

    @RequestMapping("/move")
    @ResponseBody
    public NoteResult<Object> executemove(
            Integer noteId,Integer bookId){
        System.out.println("noteId="+noteId);
        System.out.println("bookId="+bookId);
        NoteResult<Object> result = noteService.moveNote(noteId, bookId);
        return result;
    }

    @RequestMapping("/update.do")
    @ResponseBody
    public NoteResult<Object>  executeupdate(Integer noteId,String title,String body){
        NoteResult<Object> result = noteService.updateNote(noteId, title, body);
        return result;
    }

    @RequestMapping("/load_share")
    @ResponseBody
    public NoteResult<Share> executeload_share(Integer shareId){
        NoteResult<Share> result = shareService.loadShareNote(shareId);
        return result;
    }

    @RequestMapping("/uploadNoteCover.do")
    @ResponseBody
    public NoteResult<Note> uploadNoteCover(Integer noteId, String params){
        NoteResult<Note> result = noteService.addNoteCover(noteId, params);
        return result;
    }

}

