package com.itlin.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itlin.pojo.Share;
import com.itlin.service.ShareService;
import com.itlin.utils.NoteResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Controller
@RequestMapping("/share")
public class ShareNoteController {
    @Reference
    private ShareService shareService;
    @RequestMapping("/add.do")
    @ResponseBody
    public NoteResult<Object> execute(Integer noteId){
        NoteResult<Object> result = shareService.shareNote(noteId);
        return result;
    }

    @RequestMapping("/search.do")
    @ResponseBody
    public NoteResult<List<Share>> execute(String keyword){
        NoteResult<List<Share>> result = shareService.searchNote(keyword);
        return result;
    }
}

