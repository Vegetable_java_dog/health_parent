package com.itlin.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.itlin.constant.MessageConstant;
import com.itlin.entity.PageResult;
import com.itlin.entity.QueryPageBean;
import com.itlin.entity.Result;
import com.itlin.pojo.Member;
import com.itlin.pojo.MemberOrder;
import com.itlin.service.MemberService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



/**
 * 会员管理（用户管理）
 */
@RestController
@RequestMapping("/member")
public class MenberController {

    @Reference
    private MemberService memberService;

    //分页查询
    @PreAuthorize("hasAuthority('CHECKITEM_QUERY')")//权限校验
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
        PageResult pageResult = memberService.findPage(queryPageBean);
        return pageResult;
    }

    //新增
    @RequestMapping("/add")
    public Result add(@RequestBody Member member){
        try {
            memberService.add(member);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_MEMBER_FAIL);
        }
        return new Result(true, MessageConstant.ADD_MEMBER_SUCCESS);
    }

    //编辑
    @RequestMapping("/edit")
    public Result edit(@RequestBody Member member){
        try {
            memberService.edit(member);
        }catch (Exception e){
            return new Result(false,MessageConstant.EDIT_MEMBER_FAIL);
        }
        return new Result(true,MessageConstant.EDIT_MEMBER_SUCCESS);
    }

    //删除
    @RequestMapping("/deleteById")
    public Result deleteById(Integer id){
        try {
            memberService.deleteById(id);
        }catch (RuntimeException e){
            return new Result(false,e.getMessage());
        }catch (Exception e){
            return new Result(false, MessageConstant.DELETE_MEMBER_FAIL);
        }
        return new Result(true,MessageConstant.DELETE_MEMBER_SUCCESS);
    }
    
}
