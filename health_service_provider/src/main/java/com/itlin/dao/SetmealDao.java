package com.itlin.dao;

import com.github.pagehelper.Page;
import com.itlin.pojo.Setmeal;

import java.util.List;
import java.util.Map;

public interface SetmealDao {
    public void add(Setmeal setmeal);
    public void setSetmealAndCheckGroup(Map<String, Integer> map);
    Page<Setmeal> findByCondition(String queryString);

    Setmeal findById(Integer id);

    List<Integer> findCheckGroupIdsBySetmealId(Integer id);

    void deleteAssociation(Integer id);

    void edit(Setmeal setmeal);

    void deleteById(Integer id);

    List<Setmeal> findAll();

    Setmeal yiFindById(int id);

    public List<Map<String, Object>> findSetmealCount();
}