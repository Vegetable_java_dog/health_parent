package com.itlin.dao;

import com.itlin.pojo.Book;

import java.util.List;



public interface BookDao {
	//根据登录的uid查找笔记本的数据
	public List<Book> findAllBook();
	//增加笔记本的操作
	public void save(Book book);
}
