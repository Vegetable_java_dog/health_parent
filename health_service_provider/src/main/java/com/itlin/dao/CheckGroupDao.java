package com.itlin.dao;

import com.github.pagehelper.Page;
import com.itlin.pojo.CheckGroup;

import java.util.List;
import java.util.Map;

public interface CheckGroupDao {
    public void add(CheckGroup checkGroup);
    public Page<CheckGroup> selectByCondition(String queryString);
    CheckGroup findById(Integer id);
    List<Integer> findCheckItemIdsByCheckGroupId(Integer id);
    void setCheckGroupAndCheckItem(Map map);
    void deleteAssociation(Integer id);
    void edit(CheckGroup checkGroup);
    public void deleteById(Integer id);

    List<CheckGroup> findAll();
}
