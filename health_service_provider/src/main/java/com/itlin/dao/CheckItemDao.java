package com.itlin.dao;

import com.github.pagehelper.Page;
import com.itlin.pojo.CheckItem;

import java.util.List;

public interface CheckItemDao {
    public void add(CheckItem checkItem);
    public Page<CheckItem> selectByCondition(String queryString);
    public long findCountByCheckItemId(Integer checkItemId);
    public void deleteById(Integer id);
    public void edit(CheckItem checkItem);
    public List<CheckItem> findAll();
}
