package com.itlin.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itlin.dao.MemberDao;
import com.itlin.entity.PageResult;
import com.itlin.entity.QueryPageBean;
import com.itlin.pojo.Member;
import com.itlin.pojo.MemberOrder;
import com.itlin.utils.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class MemberServiceImpl implements MemberService{
    @Autowired
    private MemberDao memberDao;
    //根据手机号查询会员
    public Member findByTelephone(String telephone) {
        return memberDao.findByTelephone(telephone);
    }

    @Override
    //根据月份统计会员数量
    public List<Integer> findMemberCountByMonth(List<String> date) {
        List<Integer> list = new ArrayList<>();
        for(String m : date){//  date:YYYY.MM
            String yearS = m.substring(0,4);
            String monthS = m.substring(5);
            int year = Integer.valueOf(yearS).intValue();
            int month = Integer.valueOf(monthS).intValue();
//            System.out.println(year+"-"+month);
            String lastDayOfMonth = getLastDayOfMonth(year,month);//获取每月最后一天日期
            m = lastDayOfMonth;//格式：2020.04.31
//            System.out.println(m);
            Integer count = memberDao.findMemberCountBeforeDate(m);
            list.add(count);
        }
        return list;
    }

    //新增会员
    public void add(Member member) {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        //获取当前时间Date类型，用于注册日期
        Date time_date = null;
        try {
            time_date = sdf.parse(sdf.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        member.setRegTime(time_date);//注册日期
        member.setFileNumber(member.getIdCard());//档案编号默认为身份证号

        /*//根据身份证号获取生日
        Date birthday = null;
        try {
            birthday = sdf.parse(IDCardUtils.getBirthday(member.getIdCard()));
            System.out.println(birthday);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        member.setBirthday(birthday);
        System.out.println(member.getBirthday());*/

        //根据电话号码生成默认密码
        member.setPassword(member.getPhoneNumber());

        if(member.getPassword() != null){
            member.setPassword(MD5Utils.md5(member.getPassword()));
        }
        memberDao.add(member);
    }

    //编辑
    public void edit(Member member) {
        memberDao.edit(member);
    }

    @Override
    public void deleteById(Integer id) {
        memberDao.deleteById(id);
    }

    @Override
    public List<MemberOrder> findByMemberOrder(int memberId) {
        return memberDao.findByMemberOrder(memberId);
    }

    @Override
    public void deleteMemberOrderById(Integer id) {
        memberDao.deleteMemberOrderById(id);
    }

    //分页
    public PageResult findPage(QueryPageBean queryPageBean) {
        //开始分页-设置分页条件
        PageHelper.startPage(queryPageBean.getCurrentPage(),queryPageBean.getPageSize());
        //根据页码，长度
        Page<Member> page = memberDao.selectByCondition(queryPageBean.getQueryString());
        return new PageResult(page.getTotal(),page.getResult());
    }


    /**
     * 获得该月最后一天
     * @param year
     * @param month
     * @return
     */
    public static String getLastDayOfMonth(int year,int month){
        Calendar cal = Calendar.getInstance();
        //设置年份
        cal.set(Calendar.YEAR,year);
        //设置月份
        cal.set(Calendar.MONTH, month-1);
        //获取某月最大天数
        int lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        //设置日历中月份的最大天数
        cal.set(Calendar.DAY_OF_MONTH, lastDay);
        //格式化日期
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        String lastDayOfMonth = sdf.format(cal.getTime());
//        System.out.println("lastDayOfMonth="+lastDayOfMonth);
        return lastDayOfMonth;
    }

}
