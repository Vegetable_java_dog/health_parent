package com.itlin.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.alibaba.dubbo.config.annotation.Service;
import com.itlin.dao.NoteDao;
import com.itlin.dao.ShareDao;
import com.itlin.entity.Result;
import com.itlin.pojo.Book;
import com.itlin.pojo.Cover;
import com.itlin.pojo.Note;
import com.itlin.utils.NoteResult;
import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class NoteServiceImpl implements NoteService {
	@Autowired
	private NoteDao noteDao;
	@Autowired
	private ShareDao shareDao;
	
	public NoteResult<List<Map>> loadBookNotes(Integer bookId) {
		//返回数据集合
		List<Map> list = noteDao.findByBookId(bookId);
		//构建result
		NoteResult<List<Map>> result=new NoteResult<List<Map>>();
		result.setStatus(0);
		result.setMsg("加载笔记成功");
		result.setData(list);
		return result;
	}
	
	//单击笔记,加载笔记相关信息
	public NoteResult<Note> loadNote(Integer noteId) {
		//返回数据集合
		Note note = noteDao.findByNoteId(noteId);
		//构建result
		NoteResult<Note> result = new NoteResult<Note>();
		if(note==null) {
			result.setMsg("未找到数据!");
			result.setStatus(1);
			return result;
		}else {
			result.setStatus(0);
			result.setMsg("加载笔记信息成功");
			result.setData(note);
			return result;
		}	
		
	}
	
	//更新笔记信息（保存笔记）事件
	public NoteResult<Object> updateNote(Integer noteId, String title, String body) {
		//创建note参数
		Note note=new Note();
		note.setId(noteId);
		note.setCn_note_title(title);
		note.setCn_note_body(body);
		Timestamp time = new Timestamp(System.currentTimeMillis());
		note.setCn_note_last_modify_time(time);
		//更新数据库记录
		int rows = noteDao.updateNote(note);
		//构建result
		NoteResult<Object> result = new NoteResult<Object>();
		if(rows==1) {
			result.setStatus(0);
			result.setMsg("保存笔记成功");
			return result;
		}else {
			result.setStatus(1);
			result.setMsg("保存笔记失败");
			return result;
		}
	}
	//增加笔记事件
	public NoteResult<Note> addNote(Integer bookId, String title) {
		Note note=new Note();
		//笔记本ID
		note.setCn_notebook_id(bookId);
		//笔记本标题
		note.setCn_note_title(title);
		//笔记内容
		note.setCn_note_body("");
		//创建时间
		Timestamp time = new Timestamp(System.currentTimeMillis());
		note.setCn_note_create_time(time);
		//最后修改事件
		note.setCn_note_last_modify_time(time);
		//约定1-normal 2-delete
		note.setCn_note_status_id(1);
		//约定1-normal 2-favor 3-share
		note.setCn_note_type_id(1);
		noteDao.save(note);
		NoteResult<Note> result = new NoteResult<Note>();
		result.setStatus(0);
		result.setMsg("创建笔记成功");
		result.setData(note);
		return result;
	}
	//删除笔记事件
	public NoteResult deleteNote(Integer noteId) {
		Note note = new Note();
		note.setId(noteId);
		note.setCn_note_status_id(2);
		//更新操作
		int rows = noteDao.dynamicUpdate(note);
		//创建返回结果
		NoteResult result = new NoteResult();
		if(rows >= 1){//成功
			result.setStatus(0);
			result.setMsg("删除笔记成功");
		}else{
			result.setStatus(1);
			result.setMsg("删除笔记失败");
		}
		return result;
	}
	//转移笔记事件
	public NoteResult<Object> moveNote(Integer noteId, Integer bookId) {
		Note note = new Note();
		note.setId(noteId);//设置笔记ID
		note.setCn_notebook_id(bookId);//设置笔记本ID
//		int rows = 
//			noteDao.updateBookId(note);//更新
		int rows = noteDao.dynamicUpdate(note);
		//创建返回结果
		NoteResult result = new NoteResult();
		if(rows>=1){
			result.setStatus(0);
			result.setMsg("转移笔记成功");
		}else{
			result.setStatus(1);
			result.setMsg("转移笔记失败");
		}
		return result;
	}

	@Override
	public List<Note> findByBookName(String bookName) {
		//返回数据集合
		Book book = noteDao.findByBookName(bookName);
		return noteDao.findByBookNameId(book.getId());
	}

	public NoteResult<Note> findByNoteId(Integer noteId) {
		//返回数据集合
		System.out.println("findByNoteId_noteId=="+noteId);
		Note note = noteDao.findByNoteId(noteId);
		System.out.println(note);
		System.out.println(note.toString());
		//构建result
		NoteResult<Note> result = new NoteResult<Note>();
		if(note==null) {
			result.setMsg("未找到数据!");
			result.setStatus(1);
			return result;
		}else {
			result.setStatus(0);
			result.setMsg("加载笔记信息成功");
			result.setData(note);
			return result;
		}
	}

	@Override
	public NoteResult<Note> addNoteCover(Integer noteId, String params) {
		Note Note = new Note();
		//笔记本ID
		Note.setSrc(params);
		//笔记本标题
		Note.setId(noteId);

		try {
			noteDao.updateNoteImg(Note);
		}catch (Exception e){
			e.printStackTrace();
		}
		NoteResult result = new NoteResult();
		result.setStatus(0);
		result.setMsg("跟新封面成功");
		result.setData(Note);
		return result;
	}


}
