package com.itlin.service;


import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;

import com.alibaba.dubbo.config.annotation.Service;
import com.itlin.dao.BookDao;
import com.itlin.pojo.Book;
import com.itlin.utils.NoteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class BookServiceImpl implements BookService {
	@Autowired
	BookDao bookDao;
	//根据登录的uid查找笔记本的数据
	public NoteResult<List<Book>> loadUserBook() {
		//接受结果
		NoteResult<List<Book>> result=new NoteResult<List<Book>>();
		//查询
		List<Book> books = bookDao.findAllBook();
		
		result.setStatus(0);
		result.setMsg("查询笔记本成功");
		result.setData(books);
		return result;
	}
	//增加笔记本事件
	public NoteResult<Book> addBook(String title) {
		Book book=new Book();
		//增加笔记本名称
		book.setCn_notebook_name(title);
		//增加笔记本的创建时间
		Timestamp time = new Timestamp(System.currentTimeMillis()); 
		book.setCn_notebook_createtime(time);
		//保存笔记本
		bookDao.save(book);
		//返回结果
		NoteResult<Book> result = new NoteResult<Book>();
		result.setStatus(0);
		result.setMsg("创建笔记本成功");
		result.setData(book);
		return result;
	}

}
