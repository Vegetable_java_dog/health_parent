package com.itlin.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.itlin.dao.OrderSettingDao;
import com.itlin.pojo.OrderSetting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 预约设置服务
 */
@Service
@Transactional
public class OrderSettingServiceImpl implements OrderSettingService {
    @Autowired
    private OrderSettingDao orderSettingDao;
    //批量添加
    public void add(List<OrderSetting> list) {
        if(list != null && list.size() > 0){
            for (OrderSetting orderSetting : list) {
                //检查此数据（日期）是否存在
                long count =
                        orderSettingDao.findCountByOrderDate(orderSetting.getOrderDate());
                if(count > 0){
                    //已经存在，执行更新操作
                    orderSettingDao.editNumberByOrderDate(orderSetting);
                }else{
                    //不存在，执行添加操作
                    orderSettingDao.add(orderSetting);
                }
            }
        }
    }

    //根据日期查询预约设置数据
    public List<Map> getOrderSettingByMonth(String date) {//2020-04
        String[] splitString = date.split("-");
        int year = Integer.valueOf(splitString[0]).intValue();
        int month = Integer.valueOf(splitString[1]).intValue();
        int getday = getDaysByYearMonth(year,month);
        String numberdays = String.valueOf(getday);

/*        String dateBegin = null;
        int daymonth = getMonth(year,month);
        String daymonthstring = String.valueOf(daymonth);
        if (daymonth == 0){
            dateBegin = date + "-1";//2020-3-1
        }else {
            dateBegin = date + "-" + daymonthstring;//2020-3-1
        }*/

        String dateBegin = date + "-1";//2020-3-1
        String dateEnd = date + "-" +numberdays;//2020-3-31
        Map map = new HashMap();
        map.put("dateBegin",dateBegin);
        map.put("dateEnd",dateEnd);
        List<OrderSetting> list = orderSettingDao.getOrderSettingByMonth(map);
        List<Map> data = new ArrayList<>();
        for (OrderSetting orderSetting : list) {
            Map orderSettingMap = new HashMap();
            orderSettingMap.put("date",orderSetting.getOrderDate().getDate());//获得日期（几号）
            orderSettingMap.put("number",orderSetting.getNumber());//可预约人数
            orderSettingMap.put("reservations",orderSetting.getReservations());//已预约人数
            data.add(orderSettingMap);
        }

        return data;
    }

    //根据日期修改可预约人数
    public void editNumberByDate(OrderSetting orderSetting) {
        System.out.println(orderSetting.getOrderDate());
        long count = orderSettingDao.findCountByOrderDate(orderSetting.getOrderDate());
        System.out.println(count);
        if(count > 0){
            //当前日期已经进行了预约设置，需要进行修改操作
            orderSettingDao.editNumberByOrderDate(orderSetting);
        }else{
            //当前日期没有进行预约设置，进行添加操作
            orderSettingDao.add(orderSetting);
        }
    }

    public int getDaysByYearMonth(int year, int month) {

        Calendar a = Calendar.getInstance();
        a.set(Calendar.YEAR, year);
        a.set(Calendar.MONTH, month - 1);
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1);
        int maxDate = a.get(Calendar.DATE);
        return maxDate;
    }
/*    public int getMonth(int year, int month) {

        Calendar now = Calendar.getInstance();
        int dayyear = now.get(Calendar.YEAR);
        int daymonth = now.get(Calendar.MONTH) +1;
        if (year==dayyear && month==daymonth){
            return now.get(Calendar.DAY_OF_MONTH)+1;
        }
        return 0;
    }*/
}
