package com.itlin.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itlin.dao.PermissionDao;
import com.itlin.entity.PageResult;
import com.itlin.entity.QueryPageBean;
import com.itlin.pojo.Permission;
import com.itlin.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service(interfaceClass = PermissionService.class)
@Transactional
public class PermissionServiceImpl implements PermissionService {
    @Autowired
    private PermissionDao permissionDao;

    // 分页查询
    public PageResult pageQuery(QueryPageBean queryPageBean) {
        // 使用分页助手进行分页查询
        PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());

        // 利用条件查询数据
        Page<Permission> permissions = permissionDao.findByCondition(queryPageBean.getQueryString());
        return new PageResult(permissions.getTotal(),permissions.getResult());
    }

    // 添加一个新权限
    public void add(Permission permission) {
        permissionDao.add(permission);
    }

    // 查询所有的权限数据
    public List<Permission> findAll() {
        return permissionDao.findAll();
    }
}
