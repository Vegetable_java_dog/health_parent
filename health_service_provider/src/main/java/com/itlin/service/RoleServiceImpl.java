package com.itlin.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.itlin.dao.RoleDao;
import com.itlin.pojo.Role;
import com.itlin.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = RoleService.class)
@Transactional
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleDao roleDao;

    // 查询所有的角色数据
    public List<Role> findAll() {
        return roleDao.findAll();
    }

    // 根据角色id 查询包含的权限ids
    public List<Integer> findPermissionsByRoleId(Integer roleId) {
        return roleDao.findPermissionsByRoleId(roleId);
    }

    // 更新角色所对应的权限
    public void updatePermissionAndRoleId(Integer roleId, Integer[] permissionIds) {
        // 删除角色所对应的权限关系
        roleDao.deleteAssocication(roleId);

        // 重新建立角色与权限的对应关系
        if (permissionIds != null && permissionIds.length > 0) {
            Map<String, Integer> map = null;
            for (Integer permissionId : permissionIds) {
                map = new HashMap<>();
                map.put("roleId", roleId);
                map.put("permissionId", permissionId);
                roleDao.setPermissionAndRole(map);
            }
        }
    }
}
