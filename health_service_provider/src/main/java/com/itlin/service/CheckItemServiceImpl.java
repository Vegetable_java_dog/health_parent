package com.itlin.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itlin.constant.MessageConstant;
import com.itlin.dao.CheckItemDao;
import com.itlin.entity.PageResult;
import com.itlin.entity.QueryPageBean;
import com.itlin.pojo.CheckItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * 检查项服务
 */

@Service
@Transactional
public class CheckItemServiceImpl implements CheckItemService{
    @Autowired
    private CheckItemDao checkItemDao;
    //新增项目
    public void add(CheckItem checkItem) {
        checkItemDao.add(checkItem);

    }

    //分页
    public PageResult findPage(QueryPageBean queryPageBean) {
        //开始分页-设置分页条件
        PageHelper.startPage(queryPageBean.getCurrentPage(),queryPageBean.getPageSize());
        //根据页码，长度
        Page<CheckItem> page = checkItemDao.selectByCondition(queryPageBean.getQueryString());
        return new PageResult(page.getTotal(),page.getResult());
    }

    //根据id删除项目-如果当前检查项已经被关联到某个检查组则不能被删除
    public void deleteById(Integer id) {
        //查询是否关联到检查组
        long count = checkItemDao.findCountByCheckItemId(id);
        if (count>0){
            //已经被关联，不能删除
            throw new RuntimeException(MessageConstant.CHECKITEMHASASSOCIATION);
        }
        //未关联，继续删除
        checkItemDao.deleteById(id);
    }

    //编辑
    public void edit(CheckItem checkItem) {
        checkItemDao.edit(checkItem);
    }

    //查询所有
    @Override
    public List<CheckItem> findAll() {
        return checkItemDao.findAll();
    }


}
