package com.itlin.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itlin.dao.PermissionDao;
import com.itlin.dao.RoleDao;
import com.itlin.dao.UserDao;
import com.itlin.entity.PageResult;
import com.itlin.entity.QueryPageBean;
import com.itlin.pojo.Permission;
import com.itlin.pojo.Role;
import com.itlin.pojo.User;
import com.itlin.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private PermissionDao permissionDao;

    public User findByUsername(String username) {
        User user = userDao.findByUsername(username);
        if(user == null){
            return null;
        }
        Integer userId = user.getId();
        Set<Role> roles = roleDao.findByUserId(userId);
        if(roles != null && roles.size() > 0){
            for(Role role : roles){
                Integer roleId = role.getId();
                Set<Permission> permissions = permissionDao.findByRoleId(roleId);
                if(permissions != null && permissions.size() > 0){
                    role.setPermissions(permissions);
                }
            }
            user.setRoles(roles);
        }
        return user;
    }

    // 查询用户数据的分页查询
    public PageResult pageQuery(QueryPageBean queryPageBean) {
        // 分页助手
        PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());

        // 条件查询
        Page<User> users = userDao.findByCondition(queryPageBean.getQueryString());

        // 返回一个 分页结果对象回去
        return new PageResult(users.getTotal(), users.getResult());
    }

    // 根据用户id 查询关联的角色
    public List<Integer> findRolesByUserId(Integer userId) {
        return userDao.findRolesByUserId(userId);
    }

    // 更新用户所对应的角色
    public void updateRoleAndUserId(Integer userId, Integer[] roleIds) {
        // 删除用户所对应的角色
        userDao.deleteAssocication(userId);

        // 重新建立用户所对应的角色的对应关系
        if (roleIds != null && roleIds.length > 0) {
            Map<String, Integer> map = null;
            for (Integer roleId : roleIds) {
                map = new HashMap<>();
                map.put("userId", userId);
                map.put("roleId", roleId);
                userDao.setUserAndRole(map);
            }
        }
    }

    // 添加一个新用户
    public void add(User user) throws Exception {
        // 判断用户名是否已经存在
        int count = userDao.findUserCountByUsername(user.getUsername());
        if(count > 0){
            throw new Exception("抛出异常");
        }

        // 给密码加密
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

        user.setBirthday(DateUtils.parseString2Date(DateUtils.parseDate2String(user.getBirthday())));

        // 添加用户
        userDao.add(user);
    }
}
