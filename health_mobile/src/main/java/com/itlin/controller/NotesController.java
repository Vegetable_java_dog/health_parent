package com.itlin.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itlin.constant.MessageConstant;
import com.itlin.entity.Result;
import com.itlin.entity.ResultNote;
import com.itlin.pojo.Note;
import com.itlin.service.NoteService;
import com.itlin.utils.NoteResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 首页文章
 */
@RestController
@RequestMapping("/note")
public class NotesController {

    @Reference
    private NoteService noteService;

    /*@RequestMapping("/findByBookName.do")
    @ResponseBody//以json格式返回数据
    public NoteResult<List<Map>> findByBookName(){
        String BookName = "前台显示文章";
        NoteResult<List<Map>> result = noteService.findByBookName(BookName);
        return result;
    }*/

    @RequestMapping("/findByBookName.do")
    @ResponseBody//以json格式返回数据
    public Result findByBookName(){
        String BookName = "前台显示文章";
        try {
            List<Note> noteList = noteService.findByBookName(BookName);
            return new Result(true, MessageConstant.GET_NOTES_SUCCESS,noteList);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new Result(false,MessageConstant.GET_NOTES_FAIL);
    }

    @RequestMapping("/findByNoteId.do")
    @ResponseBody//以json格式返回数据
    public ResultNote<Note> findByNoteId(Integer noteId){
        try {
            System.out.println("NotesController_noteid="+noteId);
            NoteResult<Note> result = noteService.findByNoteId(noteId);
            return new ResultNote(true, MessageConstant.GET_NOTES_SUCCESS,result);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResultNote(false,MessageConstant.GET_NOTES_FAIL);
    }

    /*@RequestMapping("/findByNoteId.do")
    @ResponseBody//以json格式返回数据
    public NoteResult<Note> findByNoteId(Integer noteId){
        NoteResult<Note> result = noteService.findByNoteId(noteId);
        return result;
    }*/
}
