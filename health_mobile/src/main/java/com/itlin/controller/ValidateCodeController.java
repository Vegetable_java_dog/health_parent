package com.itlin.controller;

import com.aliyuncs.exceptions.ClientException;
import com.itlin.constant.MessageConstant;
import com.itlin.constant.RedisMessageConstant;
import com.itlin.entity.Result;
import com.itlin.utils.SMSUtils;
import com.itlin.utils.ValidateCodeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * 短信验证码
 */
@RestController
@RequestMapping("/validateCode")
public class ValidateCodeController {
    @Autowired
    private JedisPool jedisPool;

    //体检预约时发送手机验证码
    @RequestMapping("/send4Order")
    public Result send4Order(String telephone){
        String code = "";
        try {
            //发送短信,且获得验证码
            code = SMSUtils.startOrder(4,telephone);
        } catch (Exception e) {
            e.printStackTrace();
            //验证码发送失败
            return new Result(false, MessageConstant.SEND_VALIDATECODE_FAIL);
        }
        System.out.println("发送的手机验证码为：" + code);
        //将生成的验证码缓存到redis5分钟
        Jedis jedis = jedisPool.getResource();
        jedis.setex(telephone + RedisMessageConstant.SENDTYPE_ORDER,
                5 * 60,
                code.toString());
        jedis.close();
        //验证码发送成功
        return new Result(true,MessageConstant.SEND_VALIDATECODE_SUCCESS);
    }

    //手机快速登录时发送手机验证码
    @RequestMapping("/send4Login")
    public Result send4Login(String telephone){
        String code = "";
        try {
            //发送短信
            code = SMSUtils.startLogin(6,telephone);
        } catch (Exception e) {
            e.printStackTrace();
            //验证码发送失败
            return new Result(false, MessageConstant.SEND_VALIDATECODE_FAIL);
        }
        System.out.println("发送的手机登录验证码为：" + code);
        //将生成的验证码缓存到redis
        Jedis jedis = jedisPool.getResource();
        jedis.setex(telephone+RedisMessageConstant.SENDTYPE_LOGIN,
                5 * 60,
                code.toString());
        jedis.close();
        //验证码发送成功
        return new Result(true,MessageConstant.SEND_VALIDATECODE_SUCCESS);
    }
}
