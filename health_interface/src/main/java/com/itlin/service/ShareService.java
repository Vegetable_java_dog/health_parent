package com.itlin.service;

import com.itlin.pojo.Share;
import com.itlin.utils.NoteResult;

import java.util.List;



public interface ShareService {
	//分享功能（实际上为保存到share表)
	public NoteResult<Object> shareNote(Integer noteId);
	//搜索功能
	public NoteResult<List<Share>> searchNote(String keyword);
	//点击搜索后的收藏笔记，从而查看笔记信息
	public NoteResult<Share> loadShareNote(Integer shareId);
}
