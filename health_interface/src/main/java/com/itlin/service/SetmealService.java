package com.itlin.service;

import com.itlin.entity.PageResult;
import com.itlin.entity.QueryPageBean;
import com.itlin.pojo.Setmeal;

import java.util.List;
import java.util.Map;

/**
 * 体检套餐服务接口
 */

public interface SetmealService {
    public void add(Setmeal setmeal, Integer[] checkgroupIds);

    public PageResult pageQuery(QueryPageBean queryPageBean);

    Setmeal findById(Integer id);

    List<Integer> findCheckGroupIdsBySetmealId(Integer id);

    void edit(Setmeal setmeal, Integer[] checkgroupIds);

    void deleteById(Integer id);

    List<Setmeal> findAll();

    Setmeal yiFindById(int id);

    List<Map<String, Object>> findSetmealCount();
}
