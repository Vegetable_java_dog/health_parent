package com.itlin.service;

import com.itlin.entity.PageResult;
import com.itlin.entity.QueryPageBean;
import com.itlin.pojo.Member;
import com.itlin.pojo.MemberOrder;

import java.util.List;

public interface MemberService {
    public void add(Member member);
    public Member findByTelephone(String telephone);

    public List<Integer> findMemberCountByMonth(List<String> list);

    public PageResult findPage(QueryPageBean queryPageBean);

    public void edit(Member member);

    public void deleteById(Integer id);

    public List<MemberOrder> findByMemberOrder(int memberId);

    public void deleteMemberOrderById(Integer id);
}
