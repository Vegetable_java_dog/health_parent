package com.itlin.service;

import com.itlin.entity.Result;
import com.itlin.pojo.Cover;
import com.itlin.pojo.Note;
import com.itlin.utils.NoteResult;

import java.util.List;
import java.util.Map;


public interface NoteService {
	//根据点击的bookId，从而返回一个笔记本中的笔记
	public NoteResult<List<Map>> loadBookNotes(Integer bookId);
	//单击笔记,加载笔记相关信息
	public NoteResult<Note> loadNote(Integer noteId);
	//更新笔记信息（保存笔记）事件
	public NoteResult<Object> updateNote(Integer noteId,String title,String body);
	//增加笔记事件
	public NoteResult<Note> addNote(Integer bookId,String title);
	//删除笔记事件
	public NoteResult deleteNote(Integer noteId);
	//转移笔记事件
	public NoteResult moveNote(Integer noteId, Integer bookId);

	//查询笔记本——>前台显示文章 这个笔记本内的文章
	public List<Note> findByBookName(String bookName);

	public NoteResult<Note> findByNoteId(Integer noteId);

    public NoteResult<Note> addNoteCover(Integer noteId, String params);
}
