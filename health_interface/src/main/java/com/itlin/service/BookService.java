package com.itlin.service;

import com.itlin.pojo.Book;
import com.itlin.utils.NoteResult;

import java.util.List;



public interface BookService {
	//查询所有文章集合数据
	public NoteResult<List<Book>> loadUserBook();
	
	//增加笔记本名称
	public NoteResult<Book> addBook(String title);
}
