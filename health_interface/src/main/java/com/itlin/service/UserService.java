package com.itlin.service;

import com.itlin.entity.PageResult;
import com.itlin.entity.QueryPageBean;
import com.itlin.pojo.User;

import java.util.List;

/**
 * 用户服务接口
 */
public interface UserService {
    // 根据用户名查询用户信息
    public User findByUsername(String username);

    // 查询用户数据的分页查询
    public PageResult pageQuery(QueryPageBean queryPageBean);

    // 根据用户id 查询关联的角色
    public List<Integer> findRolesByUserId(Integer userId);

    // 更新用户所对应的角色
    public void updateRoleAndUserId(Integer userId, Integer[] roleIds);

    // 添加一个新用户
    public void add(User user) throws Exception;
}
