package com.itlin.entity;


import java.io.Serializable;

public class ResultNote<T> implements Serializable {
    private boolean flag;//执行结果，true为执行成功 false为执行失败
    private String message;//返回提示信息，主要用于页面提示信息
    private T data;//返回数据
    public ResultNote(boolean flag, String message) {
        super();
        this.flag = flag;
        this.message = message;
    }
    public ResultNote(boolean flag, String message, T data) {
        this.flag = flag;
        this.message = message;
        this.data = data;
    }
    public boolean isFlag() {
        return flag;
    }
    public void setFlag(boolean flag) {
        this.flag = flag;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public Object getData() {
        return data;
    }
    public void setData(T data) {
        this.data = data;
    }
}
