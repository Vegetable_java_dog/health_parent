package com.itlin.pojo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class Note implements Serializable{
	private Integer id;//主键id
	private String src;//图片路径
	private Integer cn_notebook_id;//笔记本id
	private Integer cn_note_status_id;//笔记本状态id
	private Integer cn_note_type_id;//文章状态id
	private String cn_note_title;//文章题目
	private String cn_note_body;//文章主题
	private Timestamp cn_note_create_time;//文章创建时间
	private Timestamp cn_note_last_modify_time;//文章最后一次修改时间

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCn_notebook_id() {
		return cn_notebook_id;
	}

	public void setCn_notebook_id(Integer cn_notebook_id) {
		this.cn_notebook_id = cn_notebook_id;
	}

	public Integer getCn_note_status_id() {
		return cn_note_status_id;
	}

	public void setCn_note_status_id(Integer cn_note_status_id) {
		this.cn_note_status_id = cn_note_status_id;
	}

	public Integer getCn_note_type_id() {
		return cn_note_type_id;
	}

	public void setCn_note_type_id(Integer cn_note_type_id) {
		this.cn_note_type_id = cn_note_type_id;
	}

	public String getCn_note_title() {
		return cn_note_title;
	}

	public void setCn_note_title(String cn_note_title) {
		this.cn_note_title = cn_note_title;
	}

	public String getCn_note_body() {
		return cn_note_body;
	}

	public void setCn_note_body(String cn_note_body) {
		this.cn_note_body = cn_note_body;
	}

	public Date getCn_note_create_time() {
		return cn_note_create_time;
	}

	public void setCn_note_create_time(Timestamp cn_note_create_time) {
		this.cn_note_create_time = cn_note_create_time;
	}

	public Date getCn_note_last_modify_time() {
		return cn_note_last_modify_time;
	}

	public void setCn_note_last_modify_time(Timestamp cn_note_last_modify_time) {
		this.cn_note_last_modify_time = cn_note_last_modify_time;
	}

	@Override
	public String toString() {
		return "Note{" +
				"id=" + id +
				", src='" + src + '\'' +
				", cn_notebook_id=" + cn_notebook_id +
				", cn_note_status_id=" + cn_note_status_id +
				", cn_note_type_id=" + cn_note_type_id +
				", cn_note_title='" + cn_note_title + '\'' +
				", cn_note_body='" + cn_note_body + '\'' +
				", cn_note_create_time=" + cn_note_create_time +
				", cn_note_last_modify_time=" + cn_note_last_modify_time +
				'}';
	}
}
