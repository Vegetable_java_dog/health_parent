package com.itlin.pojo;

import java.io.Serializable;
import java.util.Date;

public class OrderResult implements Serializable {
    private Integer id;
    private Integer orderId;//预约订单id
    private String results;//结果
    private String doctor;//医生

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getResults() {
        return results;
    }

    public void setResults(String results) {
        this.results = results;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    @Override
    public String toString() {
        return "OrderResult{" +
                "id=" + id +
                ", orderId=" + orderId +
                ", results='" + results + '\'' +
                ", doctor='" + doctor + '\'' +
                '}';
    }
}
